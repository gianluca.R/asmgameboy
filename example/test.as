.name TEST123456789012345678 # This is a comment

include common.as

.addr $100 # Entry point
	nop
	jp entry
.endaddr

.addr $40 # v-blank vector
	nop
	jp vblank
.endaddr 

.define DEFAULT_LCDC $93

entry:
	ei
	ld a, 1
	ld ($FF00+$FF), a # enable vblank interrupt
	ld a, @DEFAULT_LCDC
	ld ($FF00+$40), a
	ld a, $E4
	ld ($FF00+$47), a # BG palette
main_loop:
	jp main_loop # just stall execution
	
vblank:
	push af
	push bc
	push de
	push hl
	# copy sprite #0 and #1 to VRAM
	.invoke memcpy square_sprite @TILE_ADDR_0 16
	.invoke memcpy empty_square_sprite @TILE_ADDR_0_1 16
	
	ld hl, @BG_ADDR_0
	ld d, 20
	ld e, 18
	ld b, 1
draw_loop:
	ld a, b
	ld (hl+), a

	cp 0
	jr z, set_1
	ld b, 0
	jr continue	
set_1:
	ld b, 1
	jr continue
	
continue:
	dec d
	jr nz, draw_loop
	ld a, 12
	
-	inc hl
	dec a
	cp 0
	jr nz, -
	
	ld d, 20
	ld b, 1
	dec e
	jr nz, draw_loop
	
	pop af
	pop bc
	pop de
	pop hl
	
	reti

#EOF