# Addresses
.define TILE_ADDR_0 $8000
.define TILE_ADDR_0_1 $8010
.define TILE_ADDR_1 $8800
.define BG_ADDR_0 $9800
.define BG_ADDR_1 $9C00

# Base sprites
.addr $1000
	square_sprite:
	.byte $FF $FF $FF $FF $FF $FF $FF $FF
	.byte $FF $FF $FF $FF $FF $FF $FF $FF
	empty_square_sprite:
	.byte $00 $00 $00 $00 $00 $00 $00 $00
	.byte $00 $00 $00 $00 $00 $00 $00 $00
.endaddr

# HL: destination address
# BC: source address
# DE: length
memcpy:
	ld a, (bc)
	ld (hl+), a
	inc bc
	dec de
	
	ld a, e
	cp 0
	jp nz, memcpy
	ld a, d
	cp 0
	jp nz, memcpy
	ret
	
.macro memcpy #\1 source \2 destination \3 length
	ld bc, \1
	ld hl, \2 
	ld de, \3
	call memcpy
.endmacro
#EOF