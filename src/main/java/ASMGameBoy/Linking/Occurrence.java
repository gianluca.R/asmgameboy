package ASMGameBoy.Linking;

public class Occurrence {
    private int pos;
    private int length;

    public int getPos() {
        return pos;
    }

    public int getLength() {
        return length;
    }

    public Occurrence(int pos, int length) {
        this.pos = pos;
        this.length = length;
    }
}