package ASMGameBoy.Linking;

import ASMGameBoy.Exceptions.LabelNotFoundException;

import java.util.*;
import java.util.stream.Collectors;

public class LinkMap {

    private Map<String, List<Occurrence>> labelOccurrences;
    private Map<String, Long> labelPositions;
    private SortedSet<Long> behindLabelPositions;
    private SortedSet<Long> aheadLabelPositions;

    public Map<String, List<Occurrence>> getLabelOccurrences() {
        return labelOccurrences;
    }

    public Map<String, Long> getLabelPositions() {
        return labelPositions;
    }

    public SortedSet<Long> getBehindLabelPositions() {
        return behindLabelPositions;
    }

    public SortedSet<Long> getAheadLabelPositions() {
        return aheadLabelPositions;
    }

    public LinkMap(Set<String> labels) {
        labelOccurrences = labels
                .stream()
                .collect(Collectors.toMap(l -> l, l -> new LinkedList<>()));
        labelPositions = new HashMap<>();
        behindLabelPositions = new TreeSet<>();
        aheadLabelPositions = new TreeSet<>();
    }

    public void note(String label, int pos, int length) {
        // If label doesn't exist we get a runtime exception, that's to be expected at the moment
        if (!labelOccurrences.containsKey(label)) {
            throw new LabelNotFoundException(label);
        }
        labelOccurrences.get(label).add(new Occurrence(pos, length));
    }

    public void reportLabel(String label, long pos) {
        switch (label) {
            case "+":
                aheadLabelPositions.add(pos);
                break;
            case "-":
                behindLabelPositions.add(pos);
                break;
            default:
                labelPositions.put(label, pos);
                break;
        }
    }
}
