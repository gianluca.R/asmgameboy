package ASMGameBoy.Linking;

import ASMGameBoy.Compiling.Metadata.Metadata;
import ASMGameBoy.DynamicBuffer;
import ASMGameBoy.Exceptions.LabelNotFoundException;
import ASMGameBoy.Utils;

import java.io.IOException;
import java.util.Comparator;

public class Linker {

    public static void link(Metadata metadata) throws IOException {

        LinkMap linkMap = metadata.getLinkMap();
        for (String label : linkMap.getLabelPositions().keySet()) { // Discovered labels
            long target = linkMap.getLabelPositions().get(label);
            if (target >= 0x4000) {
                target = (target % 0x4000) + 0x4000;
            }
            for (Occurrence occ : linkMap.getLabelOccurrences().get(label)) {
                writeAddress(metadata.getBuffer(), occ, target);
            }
        }
        if (linkMap.getLabelOccurrences().containsKey("-")) {
            for (Occurrence behindOcc : linkMap.getLabelOccurrences().get("-")) {
                long target = linkMap
                        .getBehindLabelPositions()
                        .stream()
                        .filter(p -> p <= behindOcc.getPos())
                        .max(Comparator.naturalOrder())
                        .orElseThrow(() -> new LabelNotFoundException("-"));
                writeAddress(metadata.getBuffer(), behindOcc, target);
            }
        }
        if (linkMap.getLabelOccurrences().containsKey("+")) {
            for (Occurrence aheadOcc : linkMap.getLabelOccurrences().get("+")) {
                long target = linkMap
                        .getBehindLabelPositions()
                        .stream()
                        .filter(p -> p > aheadOcc.getPos())
                        .min(Comparator.naturalOrder())
                        .orElseThrow(() -> new LabelNotFoundException("+"));
                writeAddress(metadata.getBuffer(), aheadOcc, target);
            }
        }
    }

    private static void writeAddress(DynamicBuffer buffer, Occurrence addressOccurrence, long target) {
        buffer.setPosition(addressOccurrence.getPos());
        if (addressOccurrence.getLength() == 2) { // Full address
            buffer.writeBytes(Utils.intToByte(new int[]{(int) target, (int) (target >> 8)}));
        } else {
            target = target - addressOccurrence.getPos() - 1;
            buffer.writeBytes(Utils.intToByte(new int[]{(int) target}));
        }
    }

}
