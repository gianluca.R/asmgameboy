package ASMGameBoy;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class DynamicBuffer {

    private List<Byte> data;
    private int position;

    public List<Byte> getData() {
        return data;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int length() {
        return this.data.size();
    }

    public DynamicBuffer() {
        data = new ArrayList<>();
    }


    public void writeByte(byte b) {
        while (position >= data.size()) {
            data.add((byte) 0);
        }
        data.set(position++, b);
    }

    public void writeByte(int b) {
        writeByte(Utils.intToByte(b));
    }

    public void writeBytes(byte[] byteArray) {
        for (byte b : byteArray) {
            writeByte(b);
        }
    }

    public void writeString(String s) {
        writeBytes(s.getBytes());
    }

    public byte read() {
        return data.get(position++);
    }

    public void writeToFile(String fileName, boolean deleteExisting) throws IOException {
        if (deleteExisting) {
            Files.deleteIfExists(Paths.get(fileName));
        }
        RandomAccessFile fileWriter = new RandomAccessFile(fileName, "rw");
        for (Byte b : data) {
            fileWriter.writeByte(b);
        }
        fileWriter.close();
    }
}
