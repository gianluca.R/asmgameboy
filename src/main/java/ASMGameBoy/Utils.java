package ASMGameBoy;

public class Utils {
    public static byte intToByte(int v) {
        return (byte) (v & 0xFF);
    }

    public static byte[] intToByte(int[] array) {
        byte[] byteArray = new byte[array.length];
        for (int i = 0; i < array.length; i++) {
            byteArray[i] = (byte) array[i];
        }
        return byteArray;
    }

    public static byte[] intToByte(int[] array1, int[] array2) {
        int[] output = new int[array1.length + array2.length];
        System.arraycopy(array1, 0, output, 0, array1.length);
        System.arraycopy(array2, 0, output, array1.length, array2.length);
        return intToByte(output);
    }

    public static int parseNumber(String value) {
        if (value.startsWith("(") && value.endsWith(")")) {
            return parseNumber(value.substring(1, value.length() - 1));
        }
        if (value.startsWith("$")) { // base 16 number
            return Integer.parseUnsignedInt(value.substring(1), 16);
        } else {
            return Integer.parseUnsignedInt(value);
        }
    }

    public static String cleanLine(String line) {
        return line.replaceAll("(?<!\\\\)#.*", "").trim();
    }
}
