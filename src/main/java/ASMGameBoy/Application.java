package ASMGameBoy;

import ASMGameBoy.Compiling.ChecksumCalc;
import ASMGameBoy.Compiling.Compiler;
import ASMGameBoy.Compiling.Metadata.Metadata;
import ASMGameBoy.Linking.LinkMap;
import ASMGameBoy.Linking.Linker;
import ASMGameBoy.Parsing.Parser;

public class Application {
    public static void main(String[] args) {
        String fileName = "example\\test.as";

        Parser parser = new Parser(fileName);
        Metadata metadata = new Metadata();

        try {
            parser.parse();
            metadata.setLinkMap(new LinkMap(parser.getLabels()));
            Compiler.compile(parser.getOutput(), metadata);
            Linker.link(metadata);
            metadata.getBuffer().writeToFile("example\\output.gb", true);
            ChecksumCalc.update("example\\output.gb");
        } catch (Exception e) {
            e.printStackTrace();
        }

        int x = 0;
    }
}