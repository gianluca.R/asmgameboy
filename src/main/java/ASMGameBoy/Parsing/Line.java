package ASMGameBoy.Parsing;


import ASMGameBoy.Utils;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class Line {
    private String content;
    private Path path;

    public String getContent() {
        return content;
    }

    public Path getPath() {
        return path;
    }

    public Line(String content, Path path) {
        this.content = content;
        this.path = path;
    }

    public static List<Line> fromContentList(List<String> contentList, Path path) {
        return contentList.stream().map(c -> new Line(Utils.cleanLine(c), path)).collect(Collectors.toList());
    }
}
