package ASMGameBoy.Parsing;

import ASMGameBoy.Parsing.ParserOutput.Directive;
import ASMGameBoy.Parsing.ParserOutput.Instruction;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LineMatcher {

    //private static final Pattern directivePattern = Pattern.compile("^\\.((.+)\\s+(.+)|(.+))$");
    private static final Pattern directivePattern = Pattern.compile("^\\.(.+)((.+)\\s+(.+))$");
    private static final Pattern labelPattern = Pattern.compile("^(.+):$");
    private static final Pattern instructionLinePattern = Pattern.compile("^((\\+)|(-)|(.+):)?\\s*(.+)$");
    private static final Pattern instructionPattern = Pattern.compile("^((.+)\\s+(.+)\\s*,\\s*(.+)|(.+)\\s+(.+)|(.+))$");
    private static final Pattern includePattern = Pattern.compile("^include\\s+(.+)$");

    public static Directive matchDirective(String lineContent) {
        lineContent = lineContent.substring(1);
        String[] values = Arrays.stream(lineContent.split(" ")).filter(v -> v.length() > 0).toArray(String[]::new);
        if (values.length == 0) {
            throw new RuntimeException();
        }
        Directive newDirective = new Directive();
        newDirective.setName(values[0]);
        if (values.length > 1) {
            newDirective.setArguments(Arrays.copyOfRange(values, 1, values.length));
        }
        return newDirective;
    }

    public static String matchLabel(String lineContent) {
        Matcher matcher = labelPattern.matcher(lineContent);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new RuntimeException();
        }
    }

    public static Instruction matchInstructionLine(String lineContent) {
        Matcher matcher = instructionLinePattern.matcher(lineContent);
        if (matcher.find()) {
            String instruction = matcher.group(5);
            Instruction parsedInstruction = matchInstructionOnly(instructionPattern.matcher(instruction));
            matchInstructionLabelOnly(parsedInstruction, matcher);
            return parsedInstruction;
        }
        throw new RuntimeException();
    }

    public static Instruction matchInstructionOnly(Matcher matcher) {
        if (!matcher.find()) {
            throw new RuntimeException();
        }
        Instruction newInstruction = new Instruction();
        if (matcher.group(2) != null) {
            // Instruction has 2 arguments
            newInstruction.setCommand(matcher.group(2));
            newInstruction.setArg1(matcher.group(3));
            newInstruction.setArg2(matcher.group(4));
        } else if (matcher.group(5) != null) {
            // Instruction has 1 argument
            newInstruction.setCommand(matcher.group(5));
            newInstruction.setArg1(matcher.group(6));
        } else {
            // Instruction has no arguments
            newInstruction.setCommand(matcher.group(7));
        }
        return newInstruction;
    }

    public static void matchInstructionLabelOnly(Instruction instructionToLabel, Matcher lineMatcher) {
        String label = lineMatcher.group(2);
        if (label == null) {
            label = lineMatcher.group(3);
        }
        if (label == null) {
            label = lineMatcher.group(4);
        }
        if (label != null) {
            instructionToLabel.getLabels().add(label);
        }
    }

    public static String matchInclude(Line line) {
        Matcher matcher = includePattern.matcher(line.getContent());
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new RuntimeException();
        }
    }
}
