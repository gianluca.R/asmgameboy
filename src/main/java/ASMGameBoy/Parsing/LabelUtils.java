package ASMGameBoy.Parsing;

import ASMGameBoy.Parsing.ParserOutput.Instruction;

import java.util.Set;

public class LabelUtils {

    public static void updateInstructionLabel(Instruction instruction, Set<String> labelsToChange, String suffix) {
        switch (instruction.getCommand()) {
            case "JP":
            case "JR":
                if (instruction.getArg2() == null) {
                    instruction.setArg1(updateLabel(instruction.getArg1(), labelsToChange, suffix));
                }
                break;
        }
    }

    private static String updateLabel(String label, Set<String> labelsToChange, String suffix) {
        if (labelsToChange.contains(label)) {
            return label + suffix;
        }
        return label;
    }
}
