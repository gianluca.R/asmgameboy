package ASMGameBoy.Parsing.ParserOutput;

import ASMGameBoy.Compiling.Metadata.Metadata;
import ASMGameBoy.DynamicBuffer;

import java.io.IOException;
import java.util.List;

public interface SerializableOutput {

    void serialize(DynamicBuffer buffer, Metadata metadata) throws IOException;

    void apply(Metadata metadata);

    List<String> getLabels();

    void setLabels(List<String> labels);

    SerializableOutput copy();

}
