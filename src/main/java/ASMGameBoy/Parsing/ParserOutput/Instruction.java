package ASMGameBoy.Parsing.ParserOutput;

import ASMGameBoy.Compiling.Metadata.Metadata;
import ASMGameBoy.Compiling.Translation.InstructionTranslator;
import ASMGameBoy.Compiling.Translation.TranslationOutput;
import ASMGameBoy.DynamicBuffer;

import java.util.LinkedList;
import java.util.List;

public class Instruction implements SerializableOutput {

    private String command;
    private String arg1;
    private String arg2;
    private List<String> labels;

    @Override
    public List<String> getLabels() {
        return labels;
    }

    @Override
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command.toUpperCase();
    }

    public String getArg1() {
        return arg1;
    }

    public void setArg1(String arg1) {
        this.arg1 = arg1;
    }

    public String getArg2() {
        return arg2;
    }

    public void setArg2(String arg2) {
        this.arg2 = arg2;
    }

    public int length() {
        if (getArg2() != null) {
            return 3;
        }
        if (getArg1() != null) {
            return 2;
        }
        if (getCommand() != null) {
            return 1;
        }
        throw new RuntimeException("Instruction can't be empty!");
    }

    public Instruction() {
        this.labels = new LinkedList<>();
    }

    @Override
    public void serialize(DynamicBuffer buffer, Metadata metadata) {
        TranslationOutput output = InstructionTranslator.translate(this);
        if (output.getLinkedLabel() != null) {
            metadata.getLinkMap().note(
                    output.getLinkedLabel(),
                    buffer.getPosition() + output.getLinkOffset(),
                    output.getLinkLength());
        }
        for (String label : getLabels()) {
            metadata.getLinkMap().reportLabel(label, buffer.getPosition()); // We don't use a lambda because this throws an exception
        }
        buffer.writeBytes(output.getByteCode());

    }

    @Override
    public void apply(Metadata metadata) {

    }

    @Override
    public SerializableOutput copy() {
        Instruction copy = new Instruction();
        copy.setCommand(this.getCommand());
        copy.setArg1(this.getArg1());
        copy.setArg2(this.getArg2());
        copy.labels = new LinkedList<>(this.getLabels());
        return copy;
    }
}
