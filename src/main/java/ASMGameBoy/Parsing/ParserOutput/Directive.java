package ASMGameBoy.Parsing.ParserOutput;

import ASMGameBoy.Compiling.Metadata.Metadata;
import ASMGameBoy.DynamicBuffer;
import ASMGameBoy.Utils;

import java.util.LinkedList;
import java.util.List;

public class Directive implements SerializableOutput {

    private String name;
    private String[] arguments;
    private List<String> labels;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toLowerCase();
    }

    public String[] getArguments() {
        return arguments;
    }

    public void setArguments(String[] arguments) {
        this.arguments = arguments;
    }

    public String getArgument(int idx) {
        return getArguments()[idx];
    }

    /**
     * Returns whether the directive supports direct label attachment. This is useful for directives that do
     * not generate code (that natively supports labels), but that write binary data and therefore can take
     * advantage of being labellable.
     */
    public boolean supportsLabel() {
        return getName().equals("byte") || getName().equals("invoke");
    }

    @Override
    public List<String> getLabels() {
        return labels;
    }

    @Override
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public Directive() {
        this.labels = new LinkedList<>();
    }

    @Override
    public void serialize(DynamicBuffer buffer, Metadata metadata) {
        if (supportsLabel()) {
            for (String label : getLabels()) {
                metadata.getLinkMap().reportLabel(label, buffer.getPosition()); // We don't use a lambda because this throws an exception
            }
        }
        switch (this.getName()) {
            case "addr":
                metadata.setPositionBeforeAddressChange(buffer.getPosition());
                buffer.setPosition(Utils.parseNumber(this.getArgument(0)));
                break;
            case "endaddr":
                int oldPos = buffer.getPosition();
                buffer.setPosition(metadata.getPositionBeforeAddressChange());
                metadata.setPositionBeforeAddressChange(oldPos);
                break;
            case "byte":
                for (String arg : getArguments()) {
                    int parsedArg = Utils.parseNumber(arg);
                    buffer.writeByte(Utils.intToByte(parsedArg));
                }
                break;
        }
    }

    @Override
    public void apply(Metadata metadata) {
        switch (this.getName()) {
            case "name":
                metadata.getHeader().setName(this.getArgument(0));
                break;
        }
    }

    @Override
    public SerializableOutput copy() {
        Directive copy = new Directive();
        copy.setName(this.getName());
        copy.setArguments(this.getArguments());
        return copy;
    }
}
