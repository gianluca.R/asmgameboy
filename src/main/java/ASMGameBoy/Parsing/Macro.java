package ASMGameBoy.Parsing;

import ASMGameBoy.Parsing.ParserOutput.SerializableOutput;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Macro {
    private List<SerializableOutput> output;
    private Set<String> labels;
    private int useCount;

    public List<SerializableOutput> getOutput() {
        return output;
    }

    public Set<String> getLabels() {
        return labels;
    }

    public int getUseCount() {
        return useCount;
    }

    public void increaseUseCount() {
        this.useCount++;
    }

    public Macro() {
        this.output = new LinkedList<>();
        this.labels = new HashSet<>();
        this.useCount = 0;
    }
}
