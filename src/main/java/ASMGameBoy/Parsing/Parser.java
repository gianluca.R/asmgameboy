package ASMGameBoy.Parsing;

import ASMGameBoy.Exceptions.UndefinedConstantException;
import ASMGameBoy.Parsing.ParserOutput.Directive;
import ASMGameBoy.Parsing.ParserOutput.Instruction;
import ASMGameBoy.Parsing.ParserOutput.SerializableOutput;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Parser {

    public final String fileName;
    private List<Line> allLines;
    private int currentLine;
    private List<SerializableOutput> output;
    private String currentMacro;
    private Map<String, Macro> macros;
    private Map<String, String> constants;

    private Set<String> labels;

    private String nextLineLabel = null;

    public List<SerializableOutput> getOutput() {
        return output;
    }

    public Set<String> getLabels() {
        return labels;
    }

    public Parser(String fileName) {
        this.fileName = fileName;
        this.output = new ArrayList<>();
        this.labels = new HashSet<>();
        this.macros = new HashMap<>();
        this.constants = new HashMap<>();
    }

    public void parse() throws IOException {
        Path path = Paths.get(this.fileName);
        allLines = Line.fromContentList(Files.readAllLines(path), path);

        for (currentLine = 0; currentLine < allLines.size(); currentLine++) {
            parseLine(allLines.get(currentLine));
        }
    }

    private void parseLine(Line line) throws IOException {
        String lineContent = line.getContent();
        if (lineContent.length() == 0) {
            return;
        }
        if (lineContent.startsWith(".")) { // It's a directive
            parseDirective(line);
        } else if (lineContent.endsWith(":")) { // It's a label
            nextLineLabel = LineMatcher.matchLabel(lineContent);
        } else if (lineContent.toLowerCase().startsWith("include")) {
            parseInclude(line);
        } else {
            parseInstruction(line);
        }
    }

    private void attachCurrentLabel(SerializableOutput output) {
        addLabel(nextLineLabel);
        output.getLabels().add(nextLineLabel);
        nextLineLabel = null;
    }

    private void parseDirective(Line line) {
        Directive newDirective = LineMatcher.matchDirective(line.getContent());
        if (nextLineLabel != null && newDirective.supportsLabel()) {
            attachCurrentLabel(newDirective);
        }
        addOutput(newDirective);
        runPreCompileDirective(newDirective);
    }

    private void parseInclude(Line line) throws IOException {
        String file = LineMatcher.matchInclude(line);
        Path path = line.getPath().getParent().resolve(file);
        List<Line> newLines = Line.fromContentList(Files.readAllLines(path), path);
        allLines.addAll(currentLine + 1, newLines);
    }

    private void parseInstruction(Line line) {
        Instruction newInstruction = LineMatcher.matchInstructionLine(line.getContent());
        if (nextLineLabel != null) {
            attachCurrentLabel(newInstruction);
        }
        for (String label : newInstruction.getLabels()) {
            addLabel(label);
        }
        runPreCompileArgument(newInstruction);

        addOutput(newInstruction);
    }

    private void runPreCompileArgument(Instruction instruction) {
        // Replace arguments that are constants with their values
        if (instruction.getArg1() != null && instruction.getArg1().startsWith("@")) {
            String constantValue = retrieveConstant(instruction.getArg1());
            if (constantValue==null) {
                throw new UndefinedConstantException(instruction.getArg1());
            }
            instruction.setArg1(constantValue);
        }
        if (instruction.getArg2() != null && instruction.getArg2().startsWith("@")) {
            String constantValue = retrieveConstant(instruction.getArg2());
            if (constantValue==null) {
                throw new UndefinedConstantException(instruction.getArg2());
            }
            instruction.setArg2(constantValue);
        }
    }

    private String retrieveConstant(String token) {
        String constantName = token.substring(1);
        if (!constants.containsKey(constantName)) {
            return null;
        }
        return constants.get(constantName);
    }

    private void runPreCompileDirective(Directive directive) {
        switch (directive.getName()) {
            case "macro":
                currentMacro = directive.getArgument(0);
                break;
            case "endmacro":
                currentMacro = null;
                break;
            case "invoke":
                insertMacro(directive.getArguments());
                break;
            case "define":
                constants.put(directive.getArgument(0), directive.getArgument(1));
                break;
        }
    }

    private void addOutput(SerializableOutput data) {
        if (currentMacro == null) {
            output.add(data);
        } else {
            if (!macros.containsKey(currentMacro)) {
                macros.put(currentMacro, new Macro());
            }
            macros.get(currentMacro).getOutput().add(data);
        }
    }

    private void addLabel(String label) {
        if (currentMacro == null) {
            labels.add(label);
        } else {
            if (!macros.containsKey(currentMacro)) {
                macros.put(currentMacro, new Macro());
            }
            if (label.equals("-") || label.equals("+")) {
                throw new RuntimeException("Relative labels not allowed inside macros.");
            }
            macros.get(currentMacro).getLabels().add(label);
        }
    }

    private void insertMacro(String[] arguments) {
        String macroName = arguments[0];
        Macro m = macros.get(macroName);
        String macroSuffix = "@" + macroName + "." + m.getUseCount();
        for (SerializableOutput macroOutput : m.getOutput()) {
            SerializableOutput copyOutput = macroOutput.copy();
            if (copyOutput instanceof Instruction) {
                Instruction copyInstruction = (Instruction) copyOutput;
                copyInstruction.setLabels(
                        copyInstruction.getLabels()
                                .stream()
                                .map(l -> l + macroSuffix)
                                .collect(Collectors.toList()));

                LabelUtils.updateInstructionLabel(copyInstruction, m.getLabels(), macroSuffix);
                // Replace arguments
                if (copyInstruction.getArg1()!=null && copyInstruction.getArg1().startsWith("\\")) {
                    int argId = Integer.parseInt(copyInstruction.getArg1().substring(1));
                    copyInstruction.setArg1(arguments[argId]);
                }
                if (copyInstruction.getArg2()!=null && copyInstruction.getArg2().startsWith("\\")) {
                    int argId = Integer.parseInt(copyInstruction.getArg2().substring(1));
                    copyInstruction.setArg2(arguments[argId]);
                }
                runPreCompileArgument(copyInstruction);
            }
            addOutput(copyOutput);
        }
        for (String macroLabel : m.getLabels()) {
            addLabel(macroLabel + macroSuffix);
        }
        m.increaseUseCount();
    }

}
