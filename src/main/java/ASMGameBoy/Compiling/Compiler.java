package ASMGameBoy.Compiling;

import ASMGameBoy.Compiling.Metadata.Metadata;
import ASMGameBoy.DynamicBuffer;
import ASMGameBoy.Parsing.ParserOutput.SerializableOutput;

import java.io.IOException;
import java.util.List;

public class Compiler {

    public static void compile(List<SerializableOutput> data, Metadata metadata) {
        data.forEach(d -> d.apply(metadata)); // Step 1: apply all commands, so that all our structures are filled with data and ready to accept instructions
        metadata.getVectorSpace().Serialize(metadata.getBuffer());
        metadata.getBuffer().setPosition(0x100);
        metadata.getHeader().Serialize(metadata.getBuffer()); // Write header to ROM

        DynamicBuffer buffer = metadata.getBuffer();

        data.forEach(d -> {
            try {
                d.serialize(buffer, metadata);
            } catch (IOException e) {
                throw new RuntimeException();
            }
        });

        // Fill to closest power of 2
        int targetSize = (int) Math.pow(2, 15);
        int maxSize = (int) Math.pow(2, 22);
        if (metadata.getBuffer().length() < maxSize) {
            while (metadata.getBuffer().length() > targetSize) {
                targetSize *= 2;
            }
            metadata.getBuffer().setPosition(targetSize - 1);
            metadata.getBuffer().writeByte(0);
        }
    }

}
