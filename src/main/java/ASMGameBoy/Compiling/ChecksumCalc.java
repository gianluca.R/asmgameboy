package ASMGameBoy.Compiling;

import ASMGameBoy.Utils;

import java.io.IOException;
import java.io.RandomAccessFile;

public class ChecksumCalc {

    public static void update(String fileName) throws IOException {
        RandomAccessFile fileWriter = new RandomAccessFile(fileName, "rw");

        int sum = 0;
        while (fileWriter.getFilePointer() < fileWriter.length()) {
            sum = (sum + fileWriter.readUnsignedByte()) & 0xFFFF;
        }
        fileWriter.seek(0x14E);
        fileWriter.write(Utils.intToByte(new int[]{sum >> 8, sum}));

        fileWriter.close();
    }
}
