package ASMGameBoy.Compiling.Translation;

public class TranslationOutput {
    private byte[] byteCode;
    private String linkedLabel;
    private int linkOffset;
    private int linkLength;

    public byte[] getByteCode() {
        return byteCode;
    }

    public String getLinkedLabel() {
        return linkedLabel;
    }

    public int getLinkOffset() {
        return linkOffset;
    }

    public int getLinkLength() {
        return linkLength;
    }

    public TranslationOutput(byte[] byteCode) {
        this.byteCode = byteCode;
    }

    public TranslationOutput(byte[] byteCode, String linkedLabel, int linkOffset, int linkLength) {
        this(byteCode);
        this.linkedLabel = linkedLabel;
        this.linkOffset = linkOffset;
        this.linkLength = linkLength;
    }
}
