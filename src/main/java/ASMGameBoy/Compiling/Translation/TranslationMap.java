package ASMGameBoy.Compiling.Translation;

import ASMGameBoy.Exceptions.InvalidInstructionArgumentException;
import ASMGameBoy.Exceptions.UnknownInstructionException;
import ASMGameBoy.Parsing.ParserOutput.Instruction;
import ASMGameBoy.Utils;

import java.util.*;

public class TranslationMap {
    private Map<String, Map<String, Map<String, InstructionMapping>>> doubleArgumentMap;
    private Map<String, Map<String, InstructionMapping>> singleArgumentMap;
    private Map<String, InstructionMapping> noArgumentMap;

    private Set<String> registerArguments;

    public TranslationMap() {
        doubleArgumentMap = new HashMap<>();
        singleArgumentMap = new HashMap<>();
        noArgumentMap = new HashMap<>();

        registerArguments = new HashSet<>(Arrays.asList(
                "A", "B", "C", "D", "E", "H", "L",
                "Z", "NZ", "C", "NC",
                "AF", "BC", "DE", "HL", "SP",
                "(HL)", "(BC)", "(DE)", "(SP)", "(C)", "(HL+)", "(HL-)",
                "($FF00+C)"
        ));
    }

    public void addMapping(int argEncodingLength, String command, int... byteCode) {
        InstructionMapping instructionMapping = new InstructionMapping(command, null, null, byteCode, argEncodingLength);
        this.noArgumentMap.put(command, instructionMapping);
    }

    public void addMapping(String command, int... byteCode) {
        addMapping(0, command, byteCode);
    }

    public void addMapping(int argEncodingLength, String command, String arg1, int... byteCode) {
        InstructionMapping instructionMapping = new InstructionMapping(command, arg1, null, byteCode, argEncodingLength);
        if (!this.singleArgumentMap.containsKey(command)) {
            this.singleArgumentMap.put(command, new HashMap<>());
        }
        this.singleArgumentMap.get(command).put(arg1, instructionMapping);
    }

    public void addMapping(String command, String arg1, int... byteCode) {
        addMapping(0, command, arg1, byteCode);
    }

    public void addMapping(int argEncodingLength, String command, String arg1, String arg2, int... byteCode) {
        InstructionMapping instructionMapping = new InstructionMapping(command, arg1, arg2, byteCode, argEncodingLength);
        if (!this.doubleArgumentMap.containsKey(command)) {
            this.doubleArgumentMap.put(command, new HashMap<>());
        }
        if (!this.doubleArgumentMap.get(command).containsKey(arg1)) {
            this.doubleArgumentMap.get(command).put(arg1, new HashMap<>());
        }
        this.doubleArgumentMap.get(command).get(arg1).put(arg2, instructionMapping);
    }

    public void addMapping(String command, String arg1, String arg2, int... byteCode) {
        addMapping(0, command, arg1, arg2, byteCode);
    }

    public TranslationOutput translate(Instruction instruction) {
        if (instruction.length() == 1) {
            InstructionMapping mapping = noArgumentMap.getOrDefault(instruction.getCommand(), null);
            if (mapping != null) {
                return new TranslationOutput(Utils.intToByte(mapping.getByteCode()));
            }
        } else if (instruction.length() == 2) {
            return translateSingleArgumentInstruction(instruction);
        } else if (instruction.length() == 3) {
            return translateDoubleArgumentInstruction(instruction);
        }

        // If everything else fails
        throw new UnknownInstructionException(instruction);
    }

    private TranslationOutput translateSingleArgumentInstruction(Instruction instruction) {
        var possibleMappings = singleArgumentMap.get(instruction.getCommand());
        String arg1Type = getArgumentType(instruction.getArg1()); // We determine what kind of input we got (register? number? label?)

        String matchedArg1Type = // Now, if our type is not a register, we need to map it to our possible keys: number might map to nn or n, and so forth
                possibleMappings.containsKey(arg1Type) ? arg1Type : matchArgumentTypeToMapping(arg1Type, possibleMappings);

        // At this point we know what mapping we will use so we retrieve it
        InstructionMapping mapping = possibleMappings.get(matchedArg1Type);
        if (mapping == null) {
            throw new UnknownInstructionException(instruction);
        }
        // Finally, we encode our output, adding direct arguments if required
        return translateInstructionMappingWithArgument(mapping, instruction.getArg1());
    }

    private TranslationOutput translateDoubleArgumentInstruction(Instruction instruction) {
        var possibleArg1Mappings = doubleArgumentMap.get(instruction.getCommand());
        String arg1Type = getArgumentType(instruction.getArg1()); // We determine what kind of input we got (register? number? label?)

        String matchedArg1Type = // Now, if our type is not a register, we need to map it to our possible keys: number might map to nn or n, and so forth
                possibleArg1Mappings.containsKey(arg1Type) ? arg1Type : matchArgumentTypeToMapping(arg1Type, possibleArg1Mappings);

        var possibleArg2Mappings = possibleArg1Mappings.get(matchedArg1Type);
        if (possibleArg2Mappings == null) {
            throw new UnknownInstructionException(instruction);
        }
        String arg2Type = getArgumentType(instruction.getArg2()); // As before, we repeat our process for arg 2

        String matchedArg2Type =
                possibleArg2Mappings.containsKey(arg2Type) ? arg2Type : matchArgumentTypeToMapping(arg2Type, possibleArg2Mappings);

        // At this point we know what mapping we will use so we retrieve it
        InstructionMapping mapping = possibleArg2Mappings.get(matchedArg2Type);
        if (mapping == null) {
            throw new UnknownInstructionException(instruction);
        }

        // Now we have to decide, only one argument can be treated as direct input
        String argToParse = instruction.getArg1();
        if (!arg2Type.equals(matchedArg2Type)) {
            argToParse = instruction.getArg2();
        }
        return translateInstructionMappingWithArgument(mapping, argToParse);
    }

    private TranslationOutput translateInstructionMappingWithArgument(InstructionMapping mapping, String argument) {
        int[] argumentArray;
        int parsedNumber;
        int argLen = mapping.getArgumentEncodingLength();
        switch (getArgumentType(argument)) {
            case "number":
            case "(number)": // We parse these the same, so it doesn't matter which one it is
                parsedNumber = Utils.parseNumber(argument);
                argumentArray = (argLen == 1 ? new int[]{parsedNumber} : new int[]{parsedNumber, parsedNumber >> 8});
                return new TranslationOutput(Utils.intToByte(mapping.getByteCode(), argumentArray));
            case "label":
                argumentArray = (argLen == 1 ? new int[]{0} : new int[]{0, 0});
                return new TranslationOutput(Utils.intToByte(mapping.getByteCode(), argumentArray), argument, 1, argLen);
            case "(label)":
                argumentArray = (argLen == 1 ? new int[]{0} : new int[]{0, 0});
                return new TranslationOutput(Utils.intToByte(mapping.getByteCode(), argumentArray), argument.substring(1, argument.length() - 1), 1, argLen);

            case "sp+number":
                parsedNumber = Utils.parseNumber(argument.substring(3)); // We get the number only
                argumentArray = (argLen == 1 ? new int[]{parsedNumber} : new int[]{parsedNumber, parsedNumber >> 8});
                return new TranslationOutput(Utils.intToByte(mapping.getByteCode(), argumentArray));
            case "(io+number)":
                parsedNumber = Utils.parseNumber(argument.substring(7, argument.length() - 1)); // We get the number only
                argumentArray = (argLen == 1 ? new int[]{parsedNumber} : new int[]{parsedNumber, parsedNumber >> 8});
                return new TranslationOutput(Utils.intToByte(mapping.getByteCode(), argumentArray));

            default:
                return new TranslationOutput(Utils.intToByte(mapping.getByteCode()));
        }
    }

    private <T> String matchArgumentTypeToMapping(String argumentType, Map<String, T> possibleMappings) {
        if (!possibleMappings.containsKey(argumentType)) {
            switch (argumentType) {
                case "number":
                    if (possibleMappings.containsKey("nn")) {
                        return "nn";
                    }
                    if (possibleMappings.containsKey("n")) {
                        return "n";
                    }
                    break;
                case "label":
                    if (possibleMappings.containsKey("nn")) {
                        return "nn";
                    }
                    if (possibleMappings.containsKey("n")) {
                        return "n";
                    }
                    // We don't break here, next case is valid for "label" as well
                case "(label)":
                case "(number)":
                    if (possibleMappings.containsKey("(nn)")) {
                        return "(nn)";
                    }
                    break;

                case "(io+number)":
                    if (possibleMappings.containsKey("($FF00+n)")) {
                        return "($FF00+n)";
                    }
                    break;
                case "sp+number":
                    if (possibleMappings.containsKey("SP+n")) {
                        return "SP+n";
                    }
                    break;
            }
        }
        return argumentType;
    }

    private String getArgumentType(String argument) {
        //Simplest case
        if (registerArguments.contains(argument.toUpperCase())) {
            return argument.toUpperCase();
        }

        if (argument.startsWith("SP+")) {
            try {
                Utils.parseNumber(argument.substring(3));
                return "sp+number";
            } catch (NumberFormatException ignored) {
                throw new InvalidInstructionArgumentException(argument);
            }
        }

        if (argument.startsWith("($FF00+") && argument.endsWith(")")) {
            try {
                Utils.parseNumber(argument.substring(7, argument.length() - 1));
                return "(io+number)";
            } catch (NumberFormatException ignored) {
                throw new InvalidInstructionArgumentException(argument);
            }
        }

        if (argument.startsWith("(") && argument.endsWith(")")) {
            try {
                Utils.parseNumber(argument.substring(1, argument.length() - 1));
                return "(number)";
            } catch (NumberFormatException ignored) {
                return "(label)";
            }
        }

        try {
            Utils.parseNumber(argument);
            return "number";
        } catch (NumberFormatException ignored) {
            return "label";
        }
    }

    /*public TranslationOutput translate(Instruction instruction) {
        if (doubleArgumentMap.containsKey(instruction.getCommand())) {
            var commandOpCodesMap = doubleArgumentMap.get(instruction.getCommand());

            if (commandOpCodesMap.containsKey(instruction.getArg1())) {
                var commandArgumentMap = commandOpCodesMap.get(instruction.getArg1());
                return applyMap(commandArgumentMap, instruction.getArg2());
            } else {
                throw new UnknownInstructionException(instruction);
            }
        } else if (singleArgumentMap.containsKey(instruction.getCommand())) {
            var commandArgumentMap = singleArgumentMap.get(instruction.getCommand());
            return applyMap(commandArgumentMap, instruction.getArg1());
        } else if (noArgumentMap.containsKey(instruction.getCommand())) {
            return new TranslationOutput(Utils.intToByte(noArgumentMap.get(instruction.getCommand()).getByteCode()));
        }
        throw new UnknownInstructionException(instruction);
    }

    private TranslationOutput applyMap(Map<String, InstructionMapping> map, String argument) {
        if (map.containsKey(argument)) {
            return new TranslationOutput(Utils.intToByte(map.get(argument)));
        }

        if (map.containsKey("n")) { // Accepts byte direct input
            try {
                int parsedNumber = Utils.parseNumber(argument);
                if (parsedNumber <= 0xFF) {
                    return new TranslationOutput(Utils.intToByte(map.get("n"), new int[]{parsedNumber}));
                }
                // If number is valid but bigger than 255, it's not a good result
            } catch (NumberFormatException e) {
                // In this case we can assume this is a label to a short jump
                return new TranslationOutput(Utils.intToByte(map.get("n"), new int[]{0}), argument, 1, 1);
            }
        }
        if (map.containsKey("nn")) { // Accepts short direct input
            try {
                int parsedNumber = Utils.parseNumber(argument);
                if (parsedNumber <= 0xFFFF) {
                    return new TranslationOutput(Utils.intToByte(map.get("nn"), new int[]{parsedNumber, parsedNumber >> 8}));
                }
                // If number is valid but bigger than 255, it's not a good result
            } catch (NumberFormatException e) {
                // In this case this is a label to a normal 2 byte jump
                return new TranslationOutput(Utils.intToByte(map.get("nn"), new int[]{0, 0}), argument, 1, 2);
            }
        }
        throw new InvalidInstructionArgumentException(argument);
    }*/
}
