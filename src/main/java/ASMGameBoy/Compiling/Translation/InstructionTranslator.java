package ASMGameBoy.Compiling.Translation;

import ASMGameBoy.Compiling.Translation.Instructions.ArithmeticInstructions;
import ASMGameBoy.Compiling.Translation.Instructions.ControlInstructions;
import ASMGameBoy.Compiling.Translation.Instructions.JumpInstructions;
import ASMGameBoy.Compiling.Translation.Instructions.LoadInstructions;
import ASMGameBoy.Parsing.ParserOutput.Instruction;

public class InstructionTranslator {

    private static final TranslationMap translationMap;

    static {
        translationMap = new TranslationMap();

        LoadInstructions.map(translationMap);
        ArithmeticInstructions.map(translationMap);
        JumpInstructions.map(translationMap);
        ControlInstructions.map(translationMap);
    }

    public static TranslationOutput translate(Instruction instruction) {
        return translationMap.translate(instruction);
    }
}
