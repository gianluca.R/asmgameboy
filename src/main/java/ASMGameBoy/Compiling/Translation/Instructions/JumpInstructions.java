package ASMGameBoy.Compiling.Translation.Instructions;

import ASMGameBoy.Compiling.Translation.TranslationMap;

public class JumpInstructions {

    public static void map(TranslationMap translationMap) {
        translationMap.addMapping(2, "JP", "nn", 0xC3);
        translationMap.addMapping("JP", "HL", 0xE9);
        translationMap.addMapping("JP", "(HL)", 0xE9); // Alias
        translationMap.addMapping(2, "JP", "NZ", "nn", 0xC2);
        translationMap.addMapping(2, "JP", "Z", "nn", 0xCA);
        translationMap.addMapping(2, "JP", "NC", "nn", 0xD2);
        translationMap.addMapping(2, "JP", "C", "nn", 0xDA);

        translationMap.addMapping(1, "JR", "n", 0x18);
        translationMap.addMapping(1, "JR", "NZ", "nn", 0x20);
        translationMap.addMapping(1, "JR", "Z", "nn", 0x28);
        translationMap.addMapping(1, "JR", "NC", "nn", 0x30);
        translationMap.addMapping(1, "JR", "C", "nn", 0x38);

        translationMap.addMapping(2, "CALL", "nn", 0xCD);
        translationMap.addMapping(2, "CALL", "NZ", "nn", 0xC4);
        translationMap.addMapping(2, "CALL", "Z", "nn", 0xCC);
        translationMap.addMapping(2, "CALL", "NC", "nn", 0xD4);
        translationMap.addMapping(2, "CALL", "C", "nn", 0xDC);

        // TODO: RST

        translationMap.addMapping("RET", 0xC9);
        translationMap.addMapping("RET", "NZ", 0xC0);
        translationMap.addMapping("RET", "Z", 0xC8);
        translationMap.addMapping("RET", "NC", 0xD0);
        translationMap.addMapping("RET", "C", 0xD8);
        translationMap.addMapping("RETI", 0xD9);
    }

}
