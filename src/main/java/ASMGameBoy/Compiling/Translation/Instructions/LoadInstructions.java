package ASMGameBoy.Compiling.Translation.Instructions;

import ASMGameBoy.Compiling.Translation.TranslationMap;

public class LoadInstructions {

    public static void map(TranslationMap translationMap) {
        // LD A, N
        translationMap.addMapping("LD", "A", "A", 0x7F);
        translationMap.addMapping("LD", "A", "B", 0x78);
        translationMap.addMapping("LD", "A", "C", 0x79);
        translationMap.addMapping("LD", "A", "D", 0x7A);
        translationMap.addMapping("LD", "A", "E", 0x7B);
        translationMap.addMapping("LD", "A", "H", 0x7C);
        translationMap.addMapping("LD", "A", "L", 0x7D);
        translationMap.addMapping("LD", "A", "(HL)", 0x7E);
        translationMap.addMapping(1, "LD", "A", "n", 0x3E);

        // LD B, r
        translationMap.addMapping("LD", "B", "A", 0x47);
        translationMap.addMapping("LD", "B", "B", 0x40);
        translationMap.addMapping("LD", "B", "C", 0x41);
        translationMap.addMapping("LD", "B", "D", 0x42);
        translationMap.addMapping("LD", "B", "E", 0x43);
        translationMap.addMapping("LD", "B", "H", 0x44);
        translationMap.addMapping("LD", "B", "L", 0x45);
        translationMap.addMapping("LD", "B", "(HL)", 0x46);
        translationMap.addMapping(1, "LD", "B", "n", 0x06);

        // LD C, r
        translationMap.addMapping("LD", "C", "A", 0x4F);
        translationMap.addMapping("LD", "C", "B", 0x48);
        translationMap.addMapping("LD", "C", "C", 0x49);
        translationMap.addMapping("LD", "C", "D", 0x4A);
        translationMap.addMapping("LD", "C", "E", 0x4B);
        translationMap.addMapping("LD", "C", "H", 0x4C);
        translationMap.addMapping("LD", "C", "L", 0x4D);
        translationMap.addMapping("LD", "C", "(HL)", 0x4E);
        translationMap.addMapping(1, "LD", "C", "n", 0x0E);

        // LD D, r
        translationMap.addMapping("LD", "D", "A", 0x57);
        translationMap.addMapping("LD", "D", "B", 0x50);
        translationMap.addMapping("LD", "D", "C", 0x51);
        translationMap.addMapping("LD", "D", "D", 0x52);
        translationMap.addMapping("LD", "D", "E", 0x53);
        translationMap.addMapping("LD", "D", "H", 0x54);
        translationMap.addMapping("LD", "D", "L", 0x55);
        translationMap.addMapping("LD", "D", "(HL)", 0x56);
        translationMap.addMapping(1, "LD", "D", "n", 0x16);

        // LD E, r
        translationMap.addMapping("LD", "E", "A", 0x5F);
        translationMap.addMapping("LD", "E", "B", 0x58);
        translationMap.addMapping("LD", "E", "C", 0x59);
        translationMap.addMapping("LD", "E", "D", 0x5A);
        translationMap.addMapping("LD", "E", "E", 0x5B);
        translationMap.addMapping("LD", "E", "H", 0x5C);
        translationMap.addMapping("LD", "E", "L", 0x5D);
        translationMap.addMapping("LD", "E", "(HL)", 0x5E);
        translationMap.addMapping(1, "LD", "E", "n", 0x1E);

        // LD H, r
        translationMap.addMapping("LD", "H", "A", 0x67);
        translationMap.addMapping("LD", "H", "B", 0x60);
        translationMap.addMapping("LD", "H", "C", 0x61);
        translationMap.addMapping("LD", "H", "D", 0x62);
        translationMap.addMapping("LD", "H", "E", 0x63);
        translationMap.addMapping("LD", "H", "H", 0x64);
        translationMap.addMapping("LD", "H", "L", 0x65);
        translationMap.addMapping("LD", "H", "(HL)", 0x66);
        translationMap.addMapping(1, "LD", "H", "n", 0x26);

        // LD H, r
        translationMap.addMapping("LD", "L", "A", 0x6F);
        translationMap.addMapping("LD", "L", "B", 0x68);
        translationMap.addMapping("LD", "L", "C", 0x69);
        translationMap.addMapping("LD", "L", "D", 0x6A);
        translationMap.addMapping("LD", "L", "E", 0x6B);
        translationMap.addMapping("LD", "L", "H", 0x6C);
        translationMap.addMapping("LD", "L", "L", 0x6D);
        translationMap.addMapping("LD", "L", "(HL)", 0x6E);
        translationMap.addMapping(1, "LD", "L", "n", 0x2E);

        // LD (Hl), r
        translationMap.addMapping("LD", "(HL)", "A", 0x77);
        translationMap.addMapping("LD", "(HL)", "B", 0x70);
        translationMap.addMapping("LD", "(HL)", "C", 0x71);
        translationMap.addMapping("LD", "(HL)", "D", 0x72);
        translationMap.addMapping("LD", "(HL)", "E", 0x73);
        translationMap.addMapping("LD", "(HL)", "H", 0x74);
        translationMap.addMapping("LD", "(HL)", "L", 0x75);
        translationMap.addMapping(1, "LD", "(HL)", "n", 0x36);

        // LD rr, nn
        translationMap.addMapping(2, "LD", "BC", "nn", 0x01);
        translationMap.addMapping(2, "LD", "DE", "nn", 0x11);
        translationMap.addMapping(2, "LD", "HL", "nn", 0x21);
        translationMap.addMapping(2, "LD", "SP", "nn", 0x31);

        // Specials
        translationMap.addMapping("LD", "A", "(BC)", 0x0A);
        translationMap.addMapping("LD", "A", "(DE)", 0x1A);
        translationMap.addMapping("LD", "A", "(HL)", 0x7E);
        translationMap.addMapping(2, "LD", "A", "(nn)", 0xFA);

        translationMap.addMapping("LD", "(BC)", "A", 0x02);
        translationMap.addMapping("LD", "(DE)", "A", 0x12);
        translationMap.addMapping(2, "LD", "(nn)", "A", 0xEA);

        translationMap.addMapping("LD", "SP", "HL", 0xF9);
        translationMap.addMapping(1, "LD", "HL", "SP+n", 0xF8);
        translationMap.addMapping(2, "LD", "(nn)", "SP", 0x08);
        translationMap.addMapping("LD", "A", "(C)", 0xF2);
        translationMap.addMapping("LD", "($FF00+C)", "A", 0xE2);
        translationMap.addMapping("LD", "(C)", "A", 0xE2); // Alias
        translationMap.addMapping("LD", "A", "(HL-)", 0x3A);
        translationMap.addMapping("LD", "(HL-)", "A", 0x32);
        translationMap.addMapping("LD", "A", "(HL+)", 0x2A);
        translationMap.addMapping("LD", "(HL+)", "A", 0x22);
        translationMap.addMapping(1, "LD", "($FF00+n)", "A", 0xE0);
        translationMap.addMapping(1, "LD", "A", "($FF00+n)", 0xF0);

        translationMap.addMapping("PUSH", "AF", 0xF5);
        translationMap.addMapping("PUSH", "BC", 0xC5);
        translationMap.addMapping("PUSH", "DE", 0xD5);
        translationMap.addMapping("PUSH", "HL", 0xE5);

        translationMap.addMapping("POP", "AF", 0xF1);
        translationMap.addMapping("POP", "BC", 0xC1);
        translationMap.addMapping("POP", "DE", 0xD1);
        translationMap.addMapping("POP", "HL", 0xE1);
    }

}
