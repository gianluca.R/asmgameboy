package ASMGameBoy.Compiling.Translation.Instructions;

import ASMGameBoy.Compiling.Translation.TranslationMap;

public class ControlInstructions {

    public static void map(TranslationMap translationMap) {
        translationMap.addMapping("NOP", 0x00);
        translationMap.addMapping("HALT", 0x76);
        translationMap.addMapping("STOP", 0x10, 0x00);

        translationMap.addMapping("DI", 0xF3);
        translationMap.addMapping("EI", 0xFB);
    }

}
