package ASMGameBoy.Compiling.Translation.Instructions;

import ASMGameBoy.Compiling.Translation.TranslationMap;

public class ArithmeticInstructions {

    public static void map(TranslationMap translationMap) {
        mapDEC(translationMap);
        mapINC(translationMap);
        mapCP(translationMap);
        mapADD(translationMap);
        mapADC(translationMap);
        mapSUB(translationMap);
        mapSBC(translationMap);
        mapAND(translationMap);
        mapOR(translationMap);
        mapXOR(translationMap);

        translationMap.addMapping("DAA", 0x27);
    }

    public static void mapDEC(TranslationMap translationMap) {
        translationMap.addMapping("DEC", "A", 0x3D);
        translationMap.addMapping("DEC", "B", 0x05);
        translationMap.addMapping("DEC", "C", 0x0D);
        translationMap.addMapping("DEC", "D", 0x15);
        translationMap.addMapping("DEC", "E", 0x1D);
        translationMap.addMapping("DEC", "H", 0x25);
        translationMap.addMapping("DEC", "L", 0x2D);
        translationMap.addMapping("DEC", "(HL)", 0x35);

        translationMap.addMapping("DEC", "BC", 0x0B);
        translationMap.addMapping("DEC", "DE", 0x1B);
        translationMap.addMapping("DEC", "HL", 0x2B);
        translationMap.addMapping("DEC", "SP", 0x3B);
    }

    public static void mapINC(TranslationMap translationMap) {
        translationMap.addMapping("INC", "A", 0x3C);
        translationMap.addMapping("INC", "B", 0x04);
        translationMap.addMapping("INC", "C", 0x0C);
        translationMap.addMapping("INC", "D", 0x14);
        translationMap.addMapping("INC", "E", 0x1C);
        translationMap.addMapping("INC", "H", 0x24);
        translationMap.addMapping("INC", "L", 0x2C);
        translationMap.addMapping("INC", "(HL)", 0x34);

        translationMap.addMapping("INC", "BC", 0x03);
        translationMap.addMapping("INC", "DE", 0x13);
        translationMap.addMapping("INC", "HL", 0x23);
        translationMap.addMapping("INC", "SP", 0x33);
    }

    public static void mapCP(TranslationMap translationMap) {
        translationMap.addMapping("CP", "A", 0xBF);
        translationMap.addMapping("CP", "B", 0xB8);
        translationMap.addMapping("CP", "C", 0xB9);
        translationMap.addMapping("CP", "D", 0xBA);
        translationMap.addMapping("CP", "E", 0xBB);
        translationMap.addMapping("CP", "H", 0xBC);
        translationMap.addMapping("CP", "L", 0xBD);
        translationMap.addMapping("CP", "(HL)", 0xBE);
        translationMap.addMapping(1, "CP", "n", 0xFE);
    }

    public static void mapADD(TranslationMap translationMap) {
        translationMap.addMapping("ADD", "A", "A", 0x87);
        translationMap.addMapping("ADD", "A", "B", 0x80);
        translationMap.addMapping("ADD", "A", "C", 0x81);
        translationMap.addMapping("ADD", "A", "D", 0x82);
        translationMap.addMapping("ADD", "A", "E", 0x83);
        translationMap.addMapping("ADD", "A", "H", 0x84);
        translationMap.addMapping("ADD", "A", "L", 0x85);
        translationMap.addMapping("ADD", "A", "(HL)", 0x86);
        translationMap.addMapping(1, "ADD", "A", "n", 0xC6);

        translationMap.addMapping("ADD", "HL", "BC", 0x09);
        translationMap.addMapping("ADD", "HL", "DE", 0x19);
        translationMap.addMapping("ADD", "HL", "HL", 0x29);
        translationMap.addMapping("ADD", "HL", "SP", 0x39);
        translationMap.addMapping(1, "ADD", "SP", "n", 0xE8);
    }

    public static void mapADC(TranslationMap translationMap) {
        translationMap.addMapping("ADC", "A", "A", 0x8F);
        translationMap.addMapping("ADC", "A", "B", 0x88);
        translationMap.addMapping("ADC", "A", "C", 0x89);
        translationMap.addMapping("ADC", "A", "D", 0x8A);
        translationMap.addMapping("ADC", "A", "E", 0x8B);
        translationMap.addMapping("ADC", "A", "H", 0x8C);
        translationMap.addMapping("ADC", "A", "L", 0x8D);
        translationMap.addMapping("ADC", "A", "(HL)", 0x8E);
        translationMap.addMapping(1, "ADC", "A", "n", 0xCE);
    }

    public static void mapSUB(TranslationMap translationMap) {
        translationMap.addMapping("SUB", "A", "A", 0x97);
        translationMap.addMapping("SUB", "A", "B", 0x90);
        translationMap.addMapping("SUB", "A", "C", 0x91);
        translationMap.addMapping("SUB", "A", "D", 0x92);
        translationMap.addMapping("SUB", "A", "E", 0x93);
        translationMap.addMapping("SUB", "A", "H", 0x94);
        translationMap.addMapping("SUB", "A", "L", 0x95);
        translationMap.addMapping("SUB", "A", "(HL)", 0x96);
        translationMap.addMapping(1, "SUB", "A", "n", 0xD6);
    }

    public static void mapSBC(TranslationMap translationMap) {
        translationMap.addMapping("SBC", "A", "A", 0x9F);
        translationMap.addMapping("SBC", "A", "B", 0x98);
        translationMap.addMapping("SBC", "A", "C", 0x99);
        translationMap.addMapping("SBC", "A", "D", 0x9A);
        translationMap.addMapping("SBC", "A", "E", 0x9B);
        translationMap.addMapping("SBC", "A", "H", 0x9C);
        translationMap.addMapping("SBC", "A", "L", 0x9D);
        translationMap.addMapping("SBC", "A", "(HL)", 0x9E);
        translationMap.addMapping(1, "SBC", "A", "n", 0xDE);
    }

    public static void mapAND(TranslationMap translationMap) {
        translationMap.addMapping("AND", "A", 0xA7);
        translationMap.addMapping("AND", "B", 0xA0);
        translationMap.addMapping("AND", "C", 0xA1);
        translationMap.addMapping("AND", "D", 0xA2);
        translationMap.addMapping("AND", "E", 0xA3);
        translationMap.addMapping("AND", "H", 0xA4);
        translationMap.addMapping("AND", "L", 0xA5);
        translationMap.addMapping("AND", "(HL)", 0xA6);
        translationMap.addMapping(1, "AND", "n", 0xE6);
    }

    public static void mapOR(TranslationMap translationMap) {
        translationMap.addMapping("OR", "A", 0xB7);
        translationMap.addMapping("OR", "B", 0xB0);
        translationMap.addMapping("OR", "C", 0xB1);
        translationMap.addMapping("OR", "D", 0xB2);
        translationMap.addMapping("OR", "E", 0xB3);
        translationMap.addMapping("OR", "H", 0xB4);
        translationMap.addMapping("OR", "L", 0xB5);
        translationMap.addMapping("OR", "(HL)", 0xB6);
        translationMap.addMapping(1, "OR", "n", 0xF6);
    }

    public static void mapXOR(TranslationMap translationMap) {
        translationMap.addMapping("XOR", "A", 0xAF);
        translationMap.addMapping("XOR", "B", 0xA8);
        translationMap.addMapping("XOR", "C", 0xA9);
        translationMap.addMapping("XOR", "D", 0xAA);
        translationMap.addMapping("XOR", "E", 0xAB);
        translationMap.addMapping("XOR", "H", 0xAC);
        translationMap.addMapping("XOR", "L", 0xAD);
        translationMap.addMapping("XOR", "(HL)", 0xAE);
        translationMap.addMapping(1, "XOR", "n", 0xEE);
    }

}
