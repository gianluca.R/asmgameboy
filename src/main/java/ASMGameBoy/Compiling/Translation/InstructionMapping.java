package ASMGameBoy.Compiling.Translation;

public class InstructionMapping {
    private int[] byteCode;
    private int argumentEncodingLength;

    private String command;
    private String arg1;
    private String arg2;

    public int[] getByteCode() {
        return byteCode;
    }

    public int getArgumentEncodingLength() {
        return argumentEncodingLength;
    }

    public String getCommand() {
        return command;
    }

    public String getArg1() {
        return arg1;
    }

    public String getArg2() {
        return arg2;
    }

    public InstructionMapping(String command, String arg1, String arg2, int[] byteCode, int argumentEncodingLength) {
        this.byteCode = byteCode;
        this.argumentEncodingLength = argumentEncodingLength;
        this.command = command;
        this.arg1 = arg1;
        this.arg2 = arg2;
    }
}
