package ASMGameBoy.Compiling.Metadata;

import ASMGameBoy.DynamicBuffer;
import ASMGameBoy.Utils;

public class Header {

    private String name;

    public static final int NameLength = 11;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() > NameLength) {
            name = name.substring(0, NameLength);
        }
        this.name = name;
    }

    public void Serialize(DynamicBuffer buffer) {
        // Entry point
        buffer.writeBytes(new byte[]{0, 0, 0, 0});

        // Logo
        buffer.writeBytes(Utils.intToByte(new int[]{
                0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
                0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
                0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E}));

        buffer.writeString(this.getName());
        // Fill remaining space with 00s
        for (int k = 0; k < NameLength - this.getName().length(); k++) {
            buffer.writeByte(0);
        }

        // Manufacturer's code
        buffer.writeBytes(Utils.intToByte(new int[]{0xAB, 0xCD, 0xEF, 0x99}));

        // CGB flag
        buffer.writeByte(0);

        // New licensee's code
        buffer.writeBytes(new byte[]{0, 0});

        // SGB flag
        buffer.writeByte(0);

        // Cartridge type
        buffer.writeByte(0);

        // ROM size
        buffer.writeByte(0);

        // External RAM size
        buffer.writeByte(0);

        // Destination code
        buffer.writeByte(0x01);

        // Old licensee's code
        buffer.writeByte(0);

        // ROM version
        buffer.writeByte(0);

        // Header checksum
        buffer.setPosition(0x134);
        byte checksum = 0;
        while(buffer.getPosition()<=0x14C) {
            checksum = (byte)(checksum - buffer.read() - 1);
        }
        buffer.writeByte(checksum);

        // Global checksum
        buffer.writeBytes(new byte[]{0, 0});
    }
}
