package ASMGameBoy.Compiling.Metadata;

import ASMGameBoy.DynamicBuffer;

public class VectorSpace {

    // As of now this is supposed to be all empty, but more configuration options might be added later, so this is staying for now
    public void Serialize(DynamicBuffer buffer) {
        for (int k = 0; k < 100; k++) {
            buffer.writeByte(0xFF);
        }
    }

}
