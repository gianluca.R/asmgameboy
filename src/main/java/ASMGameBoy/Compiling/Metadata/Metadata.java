package ASMGameBoy.Compiling.Metadata;

import ASMGameBoy.DynamicBuffer;
import ASMGameBoy.Linking.LinkMap;

public class Metadata {
    private Header header;
    private VectorSpace vectorSpace;
    private LinkMap linkMap;
    private DynamicBuffer buffer;
    private int positionBeforeAddressChange;

    public Header getHeader() {
        return header;
    }

    public VectorSpace getVectorSpace() {
        return vectorSpace;
    }

    public LinkMap getLinkMap() {
        return linkMap;
    }

    public DynamicBuffer getBuffer() {
        return buffer;
    }

    public int getPositionBeforeAddressChange() {
        return positionBeforeAddressChange;
    }

    public void setPositionBeforeAddressChange(int positionBeforeAddressChange) {
        this.positionBeforeAddressChange = positionBeforeAddressChange;
    }

    public void setLinkMap(LinkMap linkMap) {
        this.linkMap = linkMap;
    }

    public Metadata() {
        this.header = new Header();
        this.vectorSpace = new VectorSpace();
        this.buffer = new DynamicBuffer();
    }
}
