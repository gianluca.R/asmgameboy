package ASMGameBoy.Compiling.Metadata;

import ASMGameBoy.DynamicBuffer;
import ASMGameBoy.Linking.LinkMap;

public class Macro {
    private LinkMap linkMap;
    private DynamicBuffer buffer;

    public LinkMap getLinkMap() {
        return linkMap;
    }

    public DynamicBuffer getBuffer() {
        return buffer;
    }

    public Macro(LinkMap masterMap) {
        this.linkMap = new LinkMap(masterMap.getLabelOccurrences().keySet());
        this.buffer = new DynamicBuffer();
    }
}
