package ASMGameBoy.Exceptions;

public class LabelNotFoundException extends RuntimeException {
    private String label;

    public LabelNotFoundException(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Label " + label + " not found.";
    }
}
