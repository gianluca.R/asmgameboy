package ASMGameBoy.Exceptions;

public class InvalidInstructionArgumentException extends RuntimeException {
    private String argument;

    public InvalidInstructionArgumentException(String argument) {
        this.argument = argument;
    }
}
