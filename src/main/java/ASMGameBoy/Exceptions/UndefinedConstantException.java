package ASMGameBoy.Exceptions;

public class UndefinedConstantException extends RuntimeException {
    private String constantName;

    public UndefinedConstantException(String constantName) {
        this.constantName = constantName;
    }

    @Override
    public String toString() {
        return "Constant " + constantName + " not defined.";
    }
}
