package ASMGameBoy.Exceptions;

import ASMGameBoy.Parsing.ParserOutput.Instruction;

public class UnknownInstructionException extends RuntimeException {
    private Instruction instruction;

    public UnknownInstructionException(Instruction instruction) {
        this.instruction = instruction;
    }

    @Override
    public String toString() {
        return "Instruction " + instruction.getCommand() + " " + instruction.getArg1() + " " + instruction.getArg2() + " not valid.";
    }
}
